# Model Summary

The model uses migration learning, referencing ResNet-50 from the Keras library.

After introducing the model, we discarded the original inputs to the model and replaced them with a separate fully connected layer and adjusted the input shape (32*32*3) and added the spreading and fully connected layers at the end. The official website for more parameters of the original model.


ResNet-V2 is a new residual unit proposed by the [original authors](https://arxiv.org/abs/1603.05027) that allows training to be more cheap and improves generalization. they have improved results on CIFAR-10 and CIFAR-100 using 1001 layers of ResNet and on ImageNet using 200 layers of ResNet.