# Model Summary

The model uses migration learning, referencing DenseNet121 from the Keras library.

After introducing the model, we discarded the original inputs to the model and replaced them with a separate fully connected layer and adjusted the input shape (32*32*3) and added the spreading and fully connected layers at the end. The official website for more parameters of the original model.

FractalNets repeatedly combine several sequences of parallel layers with different numbers of convolutional blocks to increase the nominal depth while maintaining a short path of forward propagation of the network. Similar operations are performed with Stochastic depth and Highway Networks.

[DenseNet](https://arxiv.org/abs/1608.06993) proposes a new model of connectivity based on this - Dense connections.

While a traditional L-layer network has only L connections, in DenseNet L(L+1)/2 connections are used. This has several obvious advantages: it avoids the problem of gradient disappearance, enhances feature propagation, enables feature reuse, and substantially reduces the number of parameters.

DenseNet outperforms most SOTA models on CIFAR-10, CIFAR-100, SVHN, and ImageNet, using fewer computations to achieve better results.

## Model Architecture

### added layers

![](../../../../images/saved_model/DenseNet/DenseNet_layers_2.png)