# Model Summary

The model uses migration learning, referencing VGG19 from the Keras library.

After introducing the model, we discarded the original inputs to the model and replaced them with a separate fully connected layer and adjusted the input shape (32*32*3) and added the spreading and fully connected layers at the end. The official website for more parameters of the original model.

ResNet, a network structure proposed by [Kaiming He](https://arxiv.org/abs/1512.03385) in 2015, won first place in the ILSVRC-2015 classification task, as well as in ImageNet detection, ImageNet localization, COCO detection and COCO segmentation. It won first place in ImageNet detection, ImageNet localization, COCO detection and COCO segmentation, which was a sensation at the time.

ResNet, also known as residual neural network, refers to the idea of adding residual learning to traditional convolutional neural networks, solving the problem of gradient dispersion and accuracy degradation (training set) in deep networks, enabling the network to go deeper and deeper, ensuring both accuracy and speed control.

## Model Architecture

![Original Architecture](./layers.md)

### Added layers

![](../../../../images/saved_model/ResNet/ResNet_layers_2.png)
