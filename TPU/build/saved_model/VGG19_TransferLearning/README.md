# Model Summary

The model uses migration learning, referencing VGG19 from the Keras library.

After introducing the model, we discarded the original inputs to the model (3 fully connected layers) and replaced them with a separate fully connected layer and adjusted the input shape (32*32*3) and added the spreading and fully connected layers at the end. The official website for more parameters of the original model.

VGG was proposed by the group of the Visual Geometry Group in Oxford. The network was the subject of related work at [ILSVRC 2014](https://arxiv.org/abs/1409.1556) and the main work was to demonstrate that increasing the depth of the network can affect the final performance of the network to some extent. There are two structures of VGG, VGG16 and VGG19, which are not fundamentally different from each other, only the depth of the network is different.

## Model Architecture

![](../../../../images/saved_model/VGG19/VGG19_layers_1.png)

### added layers

![](../../../../images/saved_model/VGG19/VGG19_layers_2.png)