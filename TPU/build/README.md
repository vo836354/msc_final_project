# Build the model

First, I built a few models and saved them in the drive. In the next few steps, we can import it and use these models for testing the robustness.

In this step, we use two techniques, one is to build CNN models directly using "sequences" in tensorflow, and the other is to use transfer learning, importing some already built models, modifying the head and tail of the models and replacing them with inputs and outputs suitable for this project.

## Hardware and Software 

### Hardware

- [ ] Google TPU or CPU

The TPU has at least an 8x speed increase

### Software

- [ ] python version: 3.7.13  [GCC 7.5.0]
- [ ] tensorflow version: 2.8.2

## Model save

In Google Colab, the most common way to store the model file is to store it as an H5 file. It's a little bit different from what we store on local PC with CPU.

If you wanna use this file, remember to change the last code to your own path.

## How to use?

Just put the file into the Google Colab, select the "TPU" as the hardware accelerator, and the Run it. 

## Chinese README
[Chinese README](./README.zh.md)