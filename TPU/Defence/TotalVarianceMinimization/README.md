# Total Variance Minimization

Total Variance Minimization implements a defence based on an image transformation attack.

## Paper introduction

[Paper](https://openreview.net/forum?id=SyJ7ClWCb)

[Original code](https://github.com/facebookarchive/adversarial_image_defenses)

## Introduction to this project(Only can used for these three)

In this project, I used Total Variance Minimization to test the defence against three types of attacks. They are:
- [ ]   FSGM
- [ ]   I-FSGM
- [ ]   DeepFool

## Formula and algorithm

Algorithm:

TotalVarMin()
- [ ] param prob: Probability of the Bernoulli distribution.
- [ ] param norm: The norm (positive integer).
- [ ] param lamb: The lambda parameter in the objective function.
- [ ] param solver: Current support: `L-BFGS-B`, `CG`, `Newton-CG`.
- [ ] param max_iter: Maximum number of iterations when performing optimization.
- [ ] param clip_values: Tuple of the form `(min, max)` representing the minimum and maximum values allowed
            for features.
- [ ] param apply_fit: True if applied during fitting/training.
- [ ] param apply_predict: True if applied during predicting.
- [ ] param verbose: Show progress bars.


## What is in this folder

All code is committed to the RACC cluster in UoR.

## Hardware and Software 

### Hardware

- [ ] CPU or GPU(Better)

### Software

- [ ] python version: 3.10.4  [GCC 7.5.0]
- [ ] tensorflow version: 2.9.1
- [ ] keras version: 2.9.0
- [ ] numpy version: 1.22.4
- [ ] adversarial-robustness-toolbox: 1.11.0
- [ ] cma version: 3.2.2

## How to use

This attack algorithm require much CPU computation and can be done quickly when using GPU.

Modified the way models are loaded. Put the model in the open repository of gitlab.

Remember to change the path of some of the code if you want to run it.


