# Madry's Protocol

## Paper introduction

[Paper](https://arxiv.org/abs/1706.06083)
[Original code](https://github.com/MadryLab/mnist_challenge)

## Introduction to this project(Only can used for these three)

In this project, I used Total Variance Minimization to test the defence against four types of attacks. They are:
- [ ]   FastGradientMethod
- [ ]   BasicIterativeMethod (I-FSGM)
- [ ]   HopSkipJump
- [ ]   PixelAttack

## Formula and algorithm

Algorithm:

AdversarialTrainerMadryPGD()
- [ ] param classifier: Classifier to train adversarially.
- [ ] param nb_epochs: Number of training epochs.
- [ ] param batch_size: Size of the batch on which adversarial samples are generated.
- [ ] param eps: Maximum perturbation that the attacker can introduce.
- [ ] param eps_step: Attack step size (input variation) at each iteration.
- [ ] param max_iter: The maximum number of iterations.
- [ ] param num_random_init: Number of random initialisations within the epsilon ball. For num_random_init=0
                                starting at the original input.


## What is in this folder

All code is committed to the RACC cluster in UoR.

## Hardware and Software 

### Hardware

- [ ] CPU or GPU(Better)

### Software

- [ ] python version: 3.10.4  [GCC 7.5.0]
- [ ] tensorflow version: 2.9.1
- [ ] keras version: 2.9.0
- [ ] numpy version: 1.22.4
- [ ] adversarial-robustness-toolbox: 1.11.0
- [ ] cma version: 3.2.2

## How to use

This attack algorithm require much CPU computation and can be done quickly when using GPU.

Modified the way models are loaded. Put the model in the open repository of gitlab.

Remember to change the path of some of the code if you want to run it.


