# Reverse Sigmoid

This defence enables a targeted defence against category probability attacks.

## Paper introduction

[Paper](https://arxiv.org/abs/1806.00054)

## Introduction to this project(Only can used for these three)

In this project, I used Total Variance Minimization to test the defence against four types of attacks. They are:
- [ ]   FastGradientMethod
- [ ]   BasicIterativeMethod (I-FSGM)
- [ ]   HopSkipJump
- [ ]   PixelAttack

## Formula and algorithm

Algorithm:

ReverseSigmoid()
- [ ] param beta: A positive magnitude parameter.
- [ ] param gamma: A positive dataset and model specific convergence parameter.
- [ ] param apply_fit: True if applied during fitting/training.
- [ ] param apply_predict: True if applied during predicting.


## What is in this folder

All code is committed to the RACC cluster in UoR.

## Hardware and Software 

### Hardware

- [ ] CPU or GPU(Better)

### Software

- [ ] python version: 3.10.4  [GCC 7.5.0]
- [ ] tensorflow version: 2.9.1
- [ ] keras version: 2.9.0
- [ ] numpy version: 1.22.4
- [ ] adversarial-robustness-toolbox: 1.11.0
- [ ] cma version: 3.2.2

## How to use

This attack algorithm require much CPU computation and can be done quickly when using GPU.

Modified the way models are loaded. Put the model in the open repository of gitlab.

Remember to change the path of some of the code if you want to run it.


