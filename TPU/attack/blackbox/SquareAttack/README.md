# Square Attack

Square Attack is a random search method that selects local square updates at random locations such that the perturbation lies approximately at the boundary of the feasible set at each iteration.

## Paper introduction

[Paper](https://arxiv.org/abs/1912.00049)

[Original code](https://github.com/max-andr/square-attack)

## Introduction to this project

They propose the Square Attack, a score-based black-box L2- and Linf-adversarial attack that does not rely on local gradient information and thus is not affected by gradient masking. Square Attack is based on a randomized search scheme which selects localized square-shaped updates at random positions so that at each iteration the perturbation is situated approximately at the boundary of the feasible set. Our method is significantly more query efficient and achieves a higher success rate compared to the state-of-the-art methods, especially in the untargeted setting. In particular, on ImageNet they improve the average query efficiency in the untargeted setting for various deep networks by a factor of at least 1.8 and up to 3 compared to the recent state-of-the-art Linf-attack of Al-Dujaili & O’Reilly. Moreover, although our attack is black-box, it can also outperform gradient-based white-box attacks on the standard benchmarks achieving a new state-of-the-art in terms of the success rate.

In this experiment, I performed separate attack detection with and without a target and examined the effect of different parameters on the l-2,l-infinite algorithm.

### Untargeted attack

for the l-2 attack, we used the following parameters:
- [ ] the number of iterations is set to 1000
- [ ] the epsilon is set to 5
- [ ] the p initialization is set to 0.8

for the l-infinite attack, we used the following parameters:
- [ ] the number of iterations is set to 1000
- [ ] the epsilon is set to 0.05
- [ ] the p initialization is set to 0.8

### targeted attack

for the l-2 attack, we used the following parameters:
- [ ] the number of iterations is set to 60000
- [ ] the epsilon is set to 5
- [ ] the p initialization is set to 0.8

for the l-infinite attack, we used the following parameters:
- [ ] the number of iterations is set to 100000
- [ ] the epsilon is set to 0.05
- [ ] the p initialization is set to 0.8

## Formula and algorithm

Formula:

![algorithm](../../../../images/attack/SquareAttack/algorithm_rs.png)

![Result](../../../../images/attack/SquareAttack/main_results_imagenet.png)

Algorithm:

PixelAttack()
- [ ] param estimator: An trained estimator.
- [ ] param norm: The norm of the adversarial perturbation. Possible values: "inf", np.inf, 1 or 2.
- [ ] param adv_criterion: The criterion which the attack should use in determining adversariality.
- [ ] param loss: The loss function which the attack should use for optimization.
- [ ] param max_iter: Maximum number of iterations.
- [ ] param eps: Maximum perturbation that the attacker can introduce.
- [ ] param p_init: Initial fraction of elements.
- [ ] param nb_restarts: Number of restarts.
- [ ] param batch_size: Batch size for estimator evaluations.
- [ ] param verbose: Show progress bars.


## What is in this folder

In this folder, depending on the name of the file, I tested the robustness of different models, under this attack.

The relevant comparison graphs were drawn and accuracy calculations were performed.

## Hardware and Software 

### Hardware

- [ ] CPU or GPU(Better)

### Software

- [ ] python version: 3.7.13  [GCC 7.5.0]
- [ ] tensorflow version: 2.8.2
- [ ] keras version: 2.8.0
- [ ] numpy version: 1.21.6
- [ ] adversarial-robustness-toolbox: 1.11.0
- [ ] cma version: 3.2.2

## How to use

This attack algorithm require much CPU computation and can be done quickly when using GPU.

Modified the way models are loaded. Put the model in the open repository of gitlab.

Remember to change the path of some of the code if you want to run it.


