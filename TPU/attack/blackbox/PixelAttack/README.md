# Pixel Attack 

Pixel Attack is a single-pixel black-box attack method proposed using differential evolution.

## Paper introduction

[Paper](https://arxiv.org/abs/1710.08864)

[Original code](https://github.com/Hyperparticle/one-pixel-attack-keras)

DNN-based methods surpass traditional image processing techniques, but recent research has found that adversarial images can allow classifiers to misclassify, by adding some perturbation to the image that is not recognisable to the human eye. Mainstream work adds perturbations that are full-image and easily recognised by the human eye, so the authors analysed extreme attack scenarios where only one pixel is changed, using a method based on differential evolution.

## Introduction to this project

Whereas the usual adversarial image is constructed by perturbing all pixels while imposing an overall constraint on the cumulative modification intensity, the minority pixel attack considered in this project is the opposite, focusing on only a few pixels but not limiting the modification intensity.

I ran separate tests for target and untarget. for the target test, we generated random tags that differed from the original tags.

## Formula and algorithm

Formula:

![](../../../../images/attack/pixel_attack/Formula.png)

Algorithm:

PixelAttack(classifier: CLASSIFIER_NEURALNETWORK_TYPE, th: int | None = None, es: int = 1, max_iter: int = 100, targeted: bool = False, verbose: bool = False)
- [ ] th: Threshold for the pixel/threshold attack. th=None indicates that a minimum threshold is found.
- [ ] es: Indicates whether the attack uses [CMA-ES](https://en.wikipedia.org/wiki/CMA-ES) (0) or [DE](https://en.wikipedia.org/wiki/Differential_evolution) (1) as an evolutionary strategy.
- [ ] max_iter: Sets the maximum number of iterations to run the evolutionary strategy for optimization.
- [ ] targeted: Indicates whether the attack is targeted (True) or untargeted (False).
- [ ] verbose: Indicates whether to print verbose information for the ES used.


## What is in this folder

In this folder, depending on the name of the file, I tested the robustness of different models, under this attack.

The relevant comparison graphs were drawn and accuracy calculations were performed.

## Hardware and Software 

### Hardware

- [ ] CPU or GPU(Better)

### Software

- [ ] python version: 3.7.13  [GCC 7.5.0]
- [ ] tensorflow version: 2.8.2
- [ ] keras version: 2.8.0
- [ ] numpy version: 1.21.6
- [ ] adversarial-robustness-toolbox: 1.11.0
- [ ] cma version: 3.2.2

## How to use

This attack algorithm require much CPU computation and can be done quickly when using GPU.

Modified the way models are loaded. Put the model in the open repository of gitlab.

Remember to change the path of some of the code if you want to run it.


