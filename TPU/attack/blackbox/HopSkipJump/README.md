# HopSkipJump Attack 

This subsection studies the HopSkipJumpAttack algorithm, which is a decision-based attack in an optimization framework, and presents a series of novel algorithms for generating targeted and untargeted adversarial examples that target " l2 -distance" or "l∞ distance " for the minimum distance is optimized. The algorithms are inherently iterative, with each iteration involving three steps: gradient direction estimation, step search via geometric steps, and boundary search via dichotomization. A theoretical analysis of the optimization framework and the gradient direction estimation is presented. This not only informs the selection of hyperparameters, but also motivates the necessary steps in the proposed algorithm.

## Paper introduction

[Paper](https://arxiv.org/abs/1904.02144)

[Original code](https://github.com/Jianbo-Lab/HSJA/)

## Introduction to this project

In this experiment, I performed separate attack detection with and without a target and examined the effect of different parameters on the l-2,l-infinite algorithm.

The effect of non-target attack of this algorithm is much better than that of target attack, which can be done after only 1000-5000 interrogations, much less than 25000 interrogations for non-target attack.

### Untargeted attack

for the l-2 attack, we used the following parameters:
- [ ] the number of iterations is set to 1000
- [ ] the epsilon is set to 5
- [ ] the p initialization is set to 0.8

for the l-infinite attack, we used the following parameters:
- [ ] the number of iterations is set to 1000
- [ ] the epsilon is set to 0.05
- [ ] the p initialization is set to 0.8

### targeted attack

for the l-2 attack, we used the following parameters:
- [ ] the number of iterations is set to 60000
- [ ] the epsilon is set to 5
- [ ] the p initialization is set to 0.8

for the l-infinite attack, we used the following parameters:
- [ ] the number of iterations is set to 100000
- [ ] the epsilon is set to 0.05
- [ ] the p initialization is set to 0.8

## Formula and algorithm

Formula:

![Bin-Search](../../../../images/attack/HopSkipJump/Bin_Search.png)

![Algorithm](../../../../images/attack/HopSkipJump/HSJ.png)

![Original-Result](../../../../images/attack/HopSkipJump/result1.png)

Algorithm:

PixelAttack()
- [ ] param classifier: A trained classifier.
- [ ] param batch_size: The size of the batch used by the estimator during inference.
- [ ] param targeted: Should the attack target one specific class.
- [ ] param norm: Order of the norm. Possible values: "inf", np.inf or 2.
- [ ] param max_iter: Maximum number of iterations. (Equivalent to the number of queries in the paper.)
- [ ] param max_eval: Maximum number of evaluations for estimating gradient. 
- [ ] param init_eval: Initial number of evaluations for estimating gradient.   (The author has set it to 100 in the source code)
- [ ] param init_size: Maximum number of trials for initial generation of adversarial examples.
- [ ] param verbose: Show progress bars.


## What is in this folder

In this folder, depending on the name of the file, I tested the robustness of different models, under this attack.

The relevant comparison graphs were drawn and accuracy calculations were performed.

## Hardware and Software 

### Hardware

- [ ] CPU or GPU(Better)

### Software

- [ ] python version: 3.7.13  [GCC 7.5.0]
- [ ] tensorflow version: 2.8.2
- [ ] keras version: 2.8.0
- [ ] numpy version: 1.21.6
- [ ] adversarial-robustness-toolbox: 1.11.0
- [ ] cma version: 3.2.2

## How to use

This attack algorithm require much CPU computation and can be done quickly when using GPU.

Modified the way models are loaded. Put the model in the open repository of gitlab.

Remember to change the path of some of the code if you want to run it.


