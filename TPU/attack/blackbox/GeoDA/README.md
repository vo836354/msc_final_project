# GeoDA

GeoDA is a very efficient and low computational black box attack algorithm.

## Paper introduction

[Paper](https://arxiv.org/abs/2003.06468)

[Original code](https://github.com/thisisalirah/GeoDA)

## Introduction to this project

In this experiment, I performed separate attack detection with and without a target and examined the effect of different parameters on the l-2,l-infinite algorithm.



## Formula and algorithm

Formula:

![Algorithm1](../../../../images/attack/GeoDA/algorithm_1.png)

![Algorithm2](../../../../images/attack/GeoDA/algorithm_2.png)

![Original-Result](../../../../images/attack/GeoDA/compare.png)

Algorithm:

GeoDA()
- [ ] param estimator: A trained classifier.
- [ ] param batch_size: The size of the batch used by the estimator during inference.
- [ ] param norm: The norm of the adversarial perturbation. Possible values: "inf", np.inf, 1 or 2.
- [ ] param sub_dim: Dimensionality of 2D frequency space (DCT).
- [ ] param max_iter: Maximum number of iterations.
- [ ] param bin_search_tol: Maximum remaining L2 perturbation defining binary search convergence. Input images are
                            normalised by maximal estimator.clip_value[1] if available or maximal value in the input
                            image.
- [ ] param lambda_param:   The lambda of equation 19 with `lambda_param=0` corresponding to a single iteration and
                            `lambda_param=1` to a uniform distribution of iterations per step.
- [ ] param sigma: Variance of the Gaussian perturbation.
- [ ] param targeted: Should the attack target one specific class.
- [ ] param verbose: Show progress bars.


## What is in this folder

All code is committed to the RACC cluster in UoR, so sub-files are distributed in /RACC/*.

## Hardware and Software 

### Hardware

- [ ] CPU or GPU(Better)

### Software

- [ ] python version: 3.10.4  [GCC 7.5.0]
- [ ] tensorflow version: 2.9.1
- [ ] keras version: 2.9.0
- [ ] numpy version: 1.22.4
- [ ] adversarial-robustness-toolbox: 1.11.0
- [ ] cma version: 3.2.2

## How to use

This attack algorithm require much CPU computation and can be done quickly when using GPU.

Modified the way models are loaded. Put the model in the open repository of gitlab.

Remember to change the path of some of the code if you want to run it.


