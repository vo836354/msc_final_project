# Jacobian-based saliency map attack (JSMA)

[JSMA](https://doi.org/10.48550/arXiv.1312.6034) introduces a method based on computing a scoring function F Jacobi matrix that iteratively manipulates the pixels that have the greatest impact on the model output and can be considered as a greedy attack algorithm.

## Formula and algorithm

Algorithm Principle:

- The JSMA algorithm consists of three main processes: calculating the forward derivative, calculating the adversarial saliency map, and adding perturbations

## What is in this folder

In this folder, depending on the name of the file, I tested the robustness of different models, under this attack.

The relevant comparison graphs were drawn and accuracy calculations were performed.

## Hardware and Software 

### Hardware

- [ ] CPU

### Software

- [ ] python version: 3.7.13  [GCC 7.5.0]
- [ ] tensorflow version: 2.8.2


## How to use

This attack algorithm does not require much CPU computation and can be done quickly locally.

Remember to change the path of some of the code if you want to run it.


