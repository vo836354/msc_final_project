# DeepFool

[Deepfool](https://arxiv.org/abs/1511.04599) is a classical adversarial attack that defines for the first time sample robustness and model robustness mirrors, and it allows accurate computation of deep classifier perturbations on large-scale datasets to reliably quantify the robustness of classifiers.

## Formula and algorithm

Formul:

![](../../../../images/attack/DeepFool/Formula.png)

Algorithm:

![](../../../../images/attack/DeepFool/algorithm1.png)

![](../../../../images/attack/DeepFool/algorithm2.png)


## What is in this folder

In this folder, depending on the name of the file, I tested the robustness of different models, under this attack.

The relevant comparison graphs were drawn and accuracy calculations were performed.

## How to use

This attack algorithm does not require much CPU computation and can be done quickly locally.

Remember to change the path of some of the code if you want to run it.


