# Fast Gradient Sign Method (FGSM)

FGSM [(Goodfellow et al., 2014)](https://arxiv.org/abs/1412.6572) searches for the direction of the fastest growing loss function in the target machine learning model.FGSM is an example of a white-box attack because the attacker needs to know the architecture and parameters of the model to perform backpropagation. Once the gradient is computed, the input can be pusheacd against a small gradient fraction.

## Formula
![](../../../../images/attack/FSGM/algorithm.png)

The FGSM formula. Here, x' is the adversarial sample that should look similar to x when ε is small, y is the output of the model. ε is a small constant that controls the size of the perturbation, and J denotes the loss function of the model.


## Althrogm


![](../../../../images/attack/FSGM/algorithm1.png)


## What is in this folder

In this folder, depending on the name of the file, I tested the robustness of different models, under this attack.

The relevant comparison graphs were drawn and accuracy calculations were performed.


## Hardware and Software 

### Hardware

- [ ] CPU

### Software

- [ ] python version: 3.7.13  [GCC 7.5.0]
- [ ] tensorflow version: 2.8.2

## How to use

This attack algorithm does not require much CPU computation and can be done quickly locally.

Remember to change the path of some of the code if you want to run it.

