# Random + FGSM (R+FGSM)

In R+FGSM, [Tramer et al., (2017)](https://arxiv.org/abs/1705.07204) suggest adding some random perturbations sampled from a Gaussian distribution before calculating the first-order derivative of the loss with respect to the input.

## Formula

![](https://gitlab.act.reading.ac.uk/vo836354/msc_final_project/-/blob/main/images/attack/whitebox/R_FSGM/logo.png)

The motivation for R + FGSM is to circumvent defenses that rely on gradient masking [(Papernot et al., 2016)](https://arxiv.org/abs/1602.02697), which is a very important concept in adversarial machine learning. Gradient masking techniques attempt to mask or hide the gradient of a model, making it more difficult for an attacker to compute the exact gradient dL/dx.


## What is in this folder

In this folder, depending on the name of the file, I tested the robustness of different models, under this attack.

The relevant comparison graphs were drawn and accuracy calculations were performed.

## Hardware and Software 

### Hardware

- [ ] CPU

### Software

- [ ] python version: 3.7.13  [GCC 7.5.0]
- [ ] tensorflow version: 2.8.2

## How to use

This attack algorithm does not require much CPU computation and can be done quickly locally.

Remember to change the path of some of the code if you want to run it.

Since this algorithm takes a long time to compute, I use Google Colab to complete the computation.

