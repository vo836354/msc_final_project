# I-FSGM

I-FSGM [(Kurakin et al., 2017)](https://arxiv.org/pdf/1607.02533.pdf) is also known as I-FSGM. BIM is an extension and improvement of FGSM, not just one calculation like FSGM, BIM performs multiple small steps of FSGM.

## What is in this folder

In this folder, depending on the name of the file, I tested the robustness of different models, under this attack.

The relevant comparison graphs were drawn and accuracy calculations were performed.


## Hardware and Software 

### Hardware

- [ ] CPU

### Software

- [ ] python version: 3.7.13  [GCC 7.5.0]
- [ ] tensorflow version: 2.8.2

## How to use

This attack algorithm does not require much CPU computation and can be done quickly locally.

Remember to change the path of some of the code if you want to run it.

