# C&W Attack

[Towards Evaluating the Robustness of Neural](https://arxiv.org/pdf/1608.04644.pdf) Networks In this paper Nicholas Carlini and David Wagner propose an attack method based on      parametrization that is more effective than previous attacks. The authors try to attack both distilled and non-distilled networks with success, thus demonstrating that distilled networks do not significantly improve the robustness of neural networks.

## Paper introduction

The authors briefly introduce Adversarial Examples and Defensive Distillation. Both upper and lower bounds can be used to evaluate the robustness of a neural network, and the CW attack in this paper is used to construct an upper bound on the robustness of the model.

In addition the authors propose to use high confidence adversarial samples to evaluate the robustness of the defence, and the CW attack produces adversarial samples that can be migrated from a non-secure model to a secure model (distillation network). Therefore, the authors argue that any model defence must be able to break this migration property.

The attacks are performed in the paper on models trained on the MNIST, CIFAR datasets and the Inception model performing the ImageNet classification task, respectively.

## Introduction to this project

Separate C&W attacks based on L0, L1 and L∞ paradigms are used to attack and test several models that have been constructed.

## Formula and algorithm

Algorithm:

This module implements the CarliniL2Method, CarliniLInfMethod, and CarliniL0Method of Carlini and Wagner's L2, LInf, and L0 optimised attacks (2016). These assaults are some of the best white-box assaults and ought to be utilised as the main assaults to assess potential defences. Line search is used in this implementation to optimise the attack objective, which is a significant change from the [previous approach](https://github.com/carlini/nn_robust_attacks).

## What is in this folder

In this folder, depending on the name of the file, I tested the robustness of different models, under this attack.

The relevant comparison graphs were drawn and accuracy calculations were performed.

## Hardware and Software 

### Hardware

- [ ] CPU or GPU(Better)

### Software

- [ ] python version: 3.7.13  [GCC 7.5.0]
- [ ] tensorflow version: 2.8.2
- [ ] keras version: 2.8.0
- [ ] numpy version: 1.21.6


## How to use

This attack algorithm require much CPU computation and can be done quickly when using GPU.

Modified the way models are loaded. Put the model in the open repository of gitlab.

Remember to change the path of some of the code if you want to run it.


