# MSc_Final_Project

![](./images/logo/logo.png)

Master's graduation project

## Getting started

This repository was used for the graduation design of the UoR Master project.

## How to use

Some of the files use the Google Colab, and others use the local PC. We recommend putting these files in the Google Colab directly, changing some of the path in code and just run it.

It is possible if you want to run all the code in the machine without TPU. But it needs you to change some codes. In Google Colab, We are running python in TPU as a cluster. Only a few code changes are needed to run it on the local CPU.

```python3
with strategy.scope():

```

For more information about google TPU, you can visit the [official website](https://www.tensorflow.org/guide/tpu). 