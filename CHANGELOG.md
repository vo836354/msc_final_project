# Change Log

## [Unreleased]

- Richer additions to the documentation

- Summary

## [0.1.2.6.3] - 2022-09-16
### Changed
- Change some image path in README.md
- Change some folder name
- Update ResNet structure.
- Upload result of SubsetScanningDetector

### Removed
- Remove a attack (Adversarial Patch)


## [0.1.2.6.2] - 2022-08-25
### Added
- A separate section has been added for testing the [accuracy](./TPU/build/acc_test/README.md) of the model.
- Temporary [new defence method](./TPU/Defence/DefensiveDistillation/DD.ipynb) to be improved.
- Correct the incorrect name in the [PixelAttack](./TPU/attack/blackbox/PixelAttack/ResNet50V2.ipynb).


## [0.1.2.6.1] - 2022-08-21
### Changed
- Some output files have been added in [Total Variance Minimization](./TPU/Defence/TotalVarianceMinimization/README.md) 


## [0.1.2.6] - 2022-08-21
### Added

- Three define have been added. Each defence is unique in its perspective and varies from one to another. They are:
- [Total Variance Minimization](./TPU/Defence/TotalVarianceMinimization/README.md) (Preprocessor)
- [Reverse Sigmoid](./TPU/Defence/ReverseSigmoid/README.md) (Postprocessor)
- [Madry's Protocol](./TPU/Defence/AdversarialTrainerMPGD/README.md) (Trainer)


## [0.1.2.5] - 2022-07-27
### Added

- Four black box attacks have been added. They are:
- [GeoDA](./TPU/attack/blackbox/GeoDA/README.md)
- [HopSkipJump](./TPU/attack/blackbox/HopSkipJump/README.md)
- [PixelAttack](./TPU/attack/blackbox/PixelAttack/README.md)
- [SquareAttack](./TPU/attack/blackbox/SquareAttack/README.md)
- However, some of the output sections are incomplete and will be added in the next release number.


## [0.1.2] - 2022-07-13
 
### Added

- For higher accuracy, a new model was added, ResNet50V2.

## [0.1.1] - 2022-07-11
 
### Added

- Getting started with the Open Source Toolkit ["adversarial-robustness-toolbox"](https://github.com/Trusted-AI/adversarial-robustness-toolbox)

- Add the C&W attack

- Delete some test file and remove duplicate files.

## [0.1.0] - 2022-07-08
 
### Added
   
- Build up 5 different ML models

- Write the source code for 4 different attack methods

- Additional documentation was added to provide a detailed description of the project.
